package org.ximinghui.telecom.core;

import org.ximinghui.telecom.core.standard.MobileNetworkCode;

import java.util.LinkedHashSet;
import java.util.Set;

public class SimpleICCIDExample {

    private static final Set<String> ICCID_SET = new LinkedHashSet<>();

    static {
        ICCID_SET.add("898604A4192180550375");
        ICCID_SET.add("8986010803592095605D");
        ICCID_SET.add("89861122148910182190");
        ICCID_SET.add("8965010812100011146");
    }

    public static void main(String[] args) {
        final String template = "%s：{行业=%s，国家/地区=%s，国际区号=%s，电信品牌=%s，电信运营商=%s}\n";
        for (String iccid : ICCID_SET) {
            SimpleICCID simpleICCID = new SimpleICCID(iccid);
            String major = simpleICCID.getIIN().getMajorIndustryIdentifier().chineseName;
            String countryOrSubdivision = simpleICCID.getIIN().getCallingCode().countryOrSubdivision.chineseName;
            String callingCode = "+" + simpleICCID.getIIN().getCallingCode().code;
            MobileNetworkCode mnc = (MobileNetworkCode) simpleICCID.getIIN().getIssuerId();
            String brand = mnc.brand.chineseName;
            String operator = mnc.operator.chineseName;
            System.out.printf(template, iccid, major, countryOrSubdivision, callingCode, brand, operator);
        }
    }

}
