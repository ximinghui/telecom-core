package org.ximinghui.telecom.core;

import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * 电子通信运营商【部分实现】
 */
@ToString
@AllArgsConstructor
public enum TelecomOperator {

    CHINA_MOBILE("China Mobile", "中国移动"),

    CHINA_UNICOM("China Unicom", "中国联通"),

    CHINA_TELECOM("China Telecom", "中国电信"),

    GLOBAL_STAR_SATELLITE("Globalstar satellite", "全球星"),

    CHINA_BROADNET("China Broadnet", "中国广电"),

    CHINA_MOBILE_TIETONG("China Mobile Tietong", "中移铁通"),

    CHINA_MOBILE_HONG_KONG("China Mobile Hong Kong Company Limited", "中国移动香港有限公司"),

    CHINA_UNICOM_HONG_KONG_LIMITED("China Unicom (Hong Kong) Limited", "中国联合网络通信（香港）股份有限公司"),

    CHINA_HONG_KONG_TELECOM("China-Hong Kong Telecom", "中港通"),

    SINGAPORE_TELECOM("Singapore Telecom", "新加坡电信");

    public final String englishName;

    public final String chineseName;

}
