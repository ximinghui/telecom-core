package org.ximinghui.telecom.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.ximinghui.telecom.core.exception.ICCIDFormatException;
import org.ximinghui.telecom.core.standard.CountryCallingCode;
import org.ximinghui.telecom.core.standard.CountryCode;
import org.ximinghui.telecom.core.standard.MobileNetworkCode;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 国际电话区号和移动网络代号解析器
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CCCAndMNCParser {

    private static final Map<String, Set<Mapper>> MAPPER = new LinkedHashMap<>();

    static {
        MobileNetworkCode[] mncArray = MobileNetworkCode.values();
        for (MobileNetworkCode mnc : mncArray) {
            String mncCode = mnc.code;
            CountryCode[] countries = mnc.mcc.countriesOrSubdivisions();
            for (CountryCode country : countries) {
                CountryCallingCode ccc;
                try {
                    ccc = CountryCallingCode.of(country);
                } catch (IllegalArgumentException e) {
                    continue;
                }
                Mapper mapper = new Mapper();
                mapper.mnc = mnc;
                mapper.ccc = ccc;
                String key = ccc.code + mncCode;
                MAPPER.putIfAbsent(key, new HashSet<>());
                MAPPER.get(key).add(mapper);
            }
        }
    }

    /**
     * 分析与给定ICCID匹配的国际电话区号和移动网络代号
     *
     * @param iccid ICCID
     * @return 匹配的国际电话区号和移动网络代号
     */
    static Set<Mapper> analyze(String iccid) {
        String subICCID = iccid.substring(2);
        Set<Mapper> matched = new HashSet<>();
        for (var entity : MAPPER.entrySet()) {
            String key = entity.getKey();
            if (!subICCID.startsWith(key)) continue;
            matched.addAll(entity.getValue());
        }
        if (matched.isEmpty()) throw new ICCIDFormatException("无法解析的ICCID：" + iccid);
        return matched;
    }

    static final class Mapper {
        private CountryCallingCode ccc;
        private MobileNetworkCode mnc;

        public CountryCallingCode getCCC() {
            return ccc;
        }

        public MobileNetworkCode getMNC() {
            return mnc;
        }
    }

}
