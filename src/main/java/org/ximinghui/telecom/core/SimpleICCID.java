package org.ximinghui.telecom.core;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.ximinghui.telecom.core.exception.ICCIDFormatException;
import org.ximinghui.telecom.core.standard.CountryCallingCode;
import org.ximinghui.telecom.core.standard.MajorIndustryIdentifier;
import org.ximinghui.telecom.core.standard.MobileNetworkCode;

/**
 * 简单的集成电路卡标识符实现
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SimpleICCID extends IntegratedCircuitCardIdentifier {

    @Getter
    protected String subICCID;

    public SimpleICCID(@NonNull String iccid) {
        // 校验ICCID参数
        if (iccid.isBlank()) throw new ICCIDFormatException("参数iccid不能为空");
        if (iccid.length() < 3) throw new ICCIDFormatException("参数iccid不合法：" + iccid);
        // 获取行业标识符
        String miiTextValue = iccid.substring(0, 2);
        MajorIndustryIdentifier mii = MajorIndustryIdentifier.of(miiTextValue);
        // 分析国际电话区号和移动网络代号（应该不会出现区号和移动网络代号冲突情况）
        CCCAndMNCParser.Mapper cccAndMNCMapper = CCCAndMNCParser.analyze(iccid).iterator().next();
        // 获取国际电话区号
        CountryCallingCode ccc = cccAndMNCMapper.getCCC();
        // 获取移动网络代号
        MobileNetworkCode mnc = cccAndMNCMapper.getMNC();
        // 初始化父类属性
        super.iin = new IssuerIdentificationNumber(mii, ccc, mnc);
        super.iai = null;
        super.checkDigit = CheckDigit.EMPTY;
        // 获取已解析的ICCID部分
        String parsedICCIDPart = "%s%s%s".formatted(miiTextValue, ccc.code, mnc.code);
        // 未解析的ICCID部分
        this.subICCID = iccid.replaceAll("^" + parsedICCIDPart, "");
    }

}
