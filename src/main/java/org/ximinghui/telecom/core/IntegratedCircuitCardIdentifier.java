package org.ximinghui.telecom.core;

import lombok.Getter;

/**
 * 集成电路卡标识符（Integrated Circuit Card Identifier）
 * <p>
 * 集成电路卡标识符依次由发行商识别号（IIN）、个人账户识别号、校验数字三部分组成。
 */
public abstract class IntegratedCircuitCardIdentifier {

    /**
     * 发行商识别号（IIN）
     */
    protected IssuerIdentificationNumber iin;

    /**
     * 个人账户识别号
     */
    protected IndividualAccountIdentification iai;

    /**
     * 校验数字
     */
    @Getter
    protected CheckDigit checkDigit;

    public IssuerIdentificationNumber getIIN() {
        return iin;
    }

    public IssuerIdentificationNumber getIssuerIdentificationNumber() {
        return getIIN();
    }

    public IndividualAccountIdentification getIAI() {
        return iai;
    }

    public IndividualAccountIdentification getIndividualAccountIdentification() {
        return getIAI();
    }

}
