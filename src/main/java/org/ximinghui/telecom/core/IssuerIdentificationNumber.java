package org.ximinghui.telecom.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.ximinghui.telecom.core.standard.CountryCallingCode;
import org.ximinghui.telecom.core.standard.MajorIndustryIdentifier;

/**
 * ICCID发行商识别号（IIN）
 * <p>
 * 发行商识别号为ICCID组成三部分中的第一部分
 */
@ToString
@AllArgsConstructor
public class IssuerIdentificationNumber {

    /**
     * 行业标识符
     */
    private MajorIndustryIdentifier mii;

    /**
     * 国家或地区
     */
    @Getter
    private CountryCallingCode callingCode;

    /**
     * 发行商标识符
     */
    @Getter
    private IssuerIdentifier issuerId;

    public MajorIndustryIdentifier getMII() {
        return mii;
    }

    public MajorIndustryIdentifier getMajorIndustryIdentifier() {
        return getMII();
    }

}
