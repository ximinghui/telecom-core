package org.ximinghui.telecom.core;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

/**
 * 校验数字
 * <p>
 * 校验数字为ICCID组成三部分中的第三部分，其为0-9的单位数值。
 * 若未采用校验数字，请保持该部分为空。中国大陆常见的三大电信
 * 运营商未采用校验数字。
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CheckDigit {

    public static final CheckDigit EMPTY = new CheckDigit(null);

    private final Character digit;

    public static CheckDigit of(Character digit) {
        if (digit == null) return EMPTY;
        if (digit >= '0' && digit <= '9') return new CheckDigit(digit);
        throw new IllegalArgumentException("参数必须为0~9的单位数值");
    }

    @Override
    public String toString() {
        if (digit == null) return "";
        return String.valueOf(digit);
    }

}
