package org.ximinghui.telecom.core;

/**
 * 个人账户识别号
 * <p>
 * 个人账户识别号为ICCID组成三部分中的第二部分，其长度是可变的，但
 * 同IIN下长度相同。个人账户识别通常为移动订阅标识号（MSIN），中国
 * 大陆常见的三大电信运营商有各自的实现规则。
 */
public interface IndividualAccountIdentification {

}
