package org.ximinghui.telecom.core;

import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * 电子通信品牌【部分实现】
 */
@ToString
@AllArgsConstructor
public enum TelecomBrand {

    CHINA_MOBILE("China Mobile", "中国移动"),

    CHINA_UNICOM("China Unicom", "中国联通"),

    CHINA_TELECOM("China Telecom", "中国电信"),

    CHINA_BROADNET("China Broadnet", "中国广电"),

    CHINA_MOBILE_TIETONG("China Mobile Tietong", "中移铁通"),

    AIRTEL("Airtel", "巴帝电信"),


    JIO("Jio", "Jio"),

    VODAFONE("Vodafone", "沃达丰"),

    ATT("AT&T", "AT&T"),

    SING_TEL("SingTel", "SingTel");

    public final String englishName;

    public final String chineseName;

}
