package org.ximinghui.telecom.core;

/**
 * 移动订阅标识号（MSIN）
 */
public interface MobileSubscriptionIdentificationNumber extends IndividualAccountIdentification {

}
