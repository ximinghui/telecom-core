package org.ximinghui.telecom.core.exception;

import lombok.experimental.StandardException;

@StandardException
public class ICCIDFormatException extends IllegalArgumentException {

}
