package org.ximinghui.telecom.core.standard;

import lombok.AllArgsConstructor;
import lombok.ToString;
import org.ximinghui.telecom.core.TelecomBrand;
import org.ximinghui.telecom.core.IssuerIdentifier;
import org.ximinghui.telecom.core.TelecomOperator;

/**
 * 移动网络代号（MNC）【部分实现】
 * <p>
 * 由国际电信联盟电信标准化部门(<a href="https://en.wikipedia.org/wiki/ITU-T">ITU-T</a>)定义
 */
@ToString
@AllArgsConstructor
public enum MobileNetworkCode implements IssuerIdentifier {

    MCC460_00(MobileCountryCode.MCC_460, "00", TelecomBrand.CHINA_MOBILE, TelecomOperator.CHINA_MOBILE),

    MCC460_01(MobileCountryCode.MCC_460, "01", TelecomBrand.CHINA_UNICOM, TelecomOperator.CHINA_UNICOM),

    MCC460_02(MobileCountryCode.MCC_460, "02", TelecomBrand.CHINA_MOBILE, TelecomOperator.CHINA_MOBILE),

    MCC460_03(MobileCountryCode.MCC_460, "03", TelecomBrand.CHINA_TELECOM, TelecomOperator.CHINA_TELECOM),

    MCC460_04(MobileCountryCode.MCC_460, "04", TelecomBrand.CHINA_MOBILE, TelecomOperator.GLOBAL_STAR_SATELLITE),

    MCC460_05(MobileCountryCode.MCC_460, "05", TelecomBrand.CHINA_TELECOM, TelecomOperator.CHINA_TELECOM),

    MCC460_06(MobileCountryCode.MCC_460, "06", TelecomBrand.CHINA_UNICOM, TelecomOperator.CHINA_UNICOM),

    MCC460_07(MobileCountryCode.MCC_460, "07", TelecomBrand.CHINA_MOBILE, TelecomOperator.CHINA_MOBILE),

    MCC460_08(MobileCountryCode.MCC_460, "08", TelecomBrand.CHINA_MOBILE, TelecomOperator.CHINA_MOBILE),

    MCC460_09(MobileCountryCode.MCC_460, "09", TelecomBrand.CHINA_UNICOM, TelecomOperator.CHINA_UNICOM),

    MCC460_11(MobileCountryCode.MCC_460, "11", TelecomBrand.CHINA_TELECOM, TelecomOperator.CHINA_TELECOM),

    MCC460_15(MobileCountryCode.MCC_460, "15", TelecomBrand.CHINA_BROADNET, TelecomOperator.CHINA_BROADNET),

    MCC460_20(MobileCountryCode.MCC_460, "20", TelecomBrand.CHINA_MOBILE_TIETONG, TelecomOperator.CHINA_MOBILE_TIETONG),

    MCC525_01(MobileCountryCode.MCC_525, "01", TelecomBrand.SING_TEL, TelecomOperator.SINGAPORE_TELECOM);

    /**
     * MCC
     */
    public final MobileCountryCode mcc;

    /**
     * MNC值
     */
    public final String code;

    /**
     * 品牌
     */
    public final TelecomBrand brand;

    /**
     * 运营商
     */
    public final TelecomOperator operator;

}
