package org.ximinghui.telecom.core.standard;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

/**
 * 行业标识符（MII）
 * <p>
 * 基于ISO-7812标准，详见：<a href="https://en.wikipedia.org/wiki/ISO/IEC_7812#Major_industry_identifier">ISO/IEC 7812 - Wikipedia</a>
 */
@ToString
@AllArgsConstructor
public enum MajorIndustryIdentifier {

    /**
     * 电子通信行业
     */
    TELECOMMUNICATION(8, 9, "电子通信");

    /**
     * <table class="striped">
     * <caption>MII首位数字含义</caption>
     * <thead>
     *     <tr><th scope="col">首位值</th><th scope="col">类别</th></tr>
     * </thead>
     * <tbody>
     *     <tr><th scope="row">0</th><td>ISO/TC68 和其它行业的分配</td></tr>
     *     <tr><th scope="row">1</th><td>航空公司</td></tr>
     *     <tr><th scope="row">2</th><td>航空公司、金融和其他的未来产业的分配</td></tr>
     *     <tr><th scope="row">3</th><td>交通和娱乐</td></tr>
     *     <tr><th scope="row">4</th><td>银行和金融</td></tr>
     *     <tr><th scope="row">5</th><td>银行和金融</td></tr>
     *     <tr><th scope="row">6</th><td>商品推广和银行/金融</td></tr>
     *     <tr><th scope="row">7</th><td>石油和其他的未来产业的分配</td></tr>
     *     <tr><th scope="row">8</th><td>医疗保健、电信和其他的未来产业的分配</td></tr>
     *     <tr><th scope="row">9</th><td>由各国国内管理机构自行分配</td></tr>
     * </tbody>
     * </table>
     */
    public final int firstDigit;

    public final int secondDigit;

    public final String chineseName;

    public static MajorIndustryIdentifier of(@NonNull String twoDigits) {
        if (twoDigits.length() != 2) throw new IllegalArgumentException("参数twoDigits格式不正确：" + twoDigits);
        char[] charArray = twoDigits.toCharArray();
        int firstDigit = Integer.parseInt(String.valueOf(charArray[0]));
        int secondDigit = Integer.parseInt(String.valueOf(charArray[1]));
        for (MajorIndustryIdentifier mii : MajorIndustryIdentifier.values()) {
            if (mii.firstDigit != firstDigit) continue;
            if (mii.secondDigit == secondDigit) return mii;
        }
        throw new IllegalArgumentException("参数twoDigits无法识别：" + twoDigits);
    }

    public String textValue() {
        return "%d%d".formatted(firstDigit, secondDigit);
    }

}
