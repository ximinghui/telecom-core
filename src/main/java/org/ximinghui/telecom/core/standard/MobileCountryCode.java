package org.ximinghui.telecom.core.standard;

import lombok.ToString;

/**
 * 移动设备国家代号（MCC）
 * <p>
 * 由国际电信联盟电信标准化部门(<a href="https://en.wikipedia.org/wiki/ITU-T">ITU-T</a>)定义，详见<a href="https://en.wikipedia.org/wiki/Mobile_country_code">维基百科</a>
 */
@ToString
public enum MobileCountryCode {

    // Test networks

    /**
     * 测试网络
     */
    MCC_001("001"),

    /**
     * 测试网络
     */
    MCC_999("999"),

    // National operators

    /**
     * 安道尔
     */
    MCC_213("213", CountryCode.AD),

    /**
     * 阿拉伯联合酋长国
     */
    MCC_424("424", CountryCode.AE),

    /**
     * 阿拉伯联合酋长国
     */
    MCC_430("430", CountryCode.AE),

    /**
     * 阿拉伯联合酋长国
     */
    MCC_431("431", CountryCode.AE),

    /**
     * 阿富汗
     */
    MCC_412("412", CountryCode.AF),

    /**
     * 安提瓜和巴布达
     */
    MCC_344("344", CountryCode.AG),

    /**
     * 注释未完善
     */
    MCC_365("365", CountryCode.AI),

    /**
     * 注释未完善
     */
    MCC_276("276", CountryCode.AL),

    /**
     * 注释未完善
     */
    MCC_283("283", CountryCode.AM),

    /**
     * 注释未完善
     */
    MCC_631("631", CountryCode.AO),

    /**
     * 注释未完善
     */
    MCC_722("722", CountryCode.AR),

    /**
     * 注释未完善
     */
    MCC_544("544", CountryCode.AS),

    /**
     * 注释未完善
     */
    MCC_232("232", CountryCode.AT),

    /**
     * 注释未完善
     */
    MCC_505("505", CountryCode.AU, CountryCode.NF),

    /**
     * 注释未完善
     */
    MCC_363("363", CountryCode.AW),

    /**
     * 注释未完善
     */
    MCC_400("400", CountryCode.AZ),

    /**
     * 注释未完善
     */
    MCC_218("218", CountryCode.BA),

    /**
     * 注释未完善
     */
    MCC_342("342", CountryCode.BB),

    /**
     * 注释未完善
     */
    MCC_470("470", CountryCode.BD),

    /**
     * 注释未完善
     */
    MCC_206("206", CountryCode.BE),

    /**
     * 注释未完善
     */
    MCC_613("613", CountryCode.BF),

    /**
     * 注释未完善
     */
    MCC_284("284", CountryCode.BG),

    /**
     * 注释未完善
     */
    MCC_426("426", CountryCode.BH),

    /**
     * 注释未完善
     */
    MCC_642("642", CountryCode.BI),

    /**
     * 注释未完善
     */
    MCC_616("616", CountryCode.BJ),

    /**
     * 注释未完善
     */
    MCC_340("340", CountryCode.BL, CountryCode.GP, CountryCode.MF, CountryCode.MQ),

    /**
     * 注释未完善
     */
    MCC_350("350", CountryCode.BM),

    /**
     * 注释未完善
     */
    MCC_528("528", CountryCode.BN),

    /**
     * 注释未完善
     */
    MCC_736("736", CountryCode.BO),

    /**
     * 注释未完善
     */
    MCC_362("362", CountryCode.BQ, CountryCode.CW, CountryCode.SX),

    /**
     * 注释未完善
     */
    MCC_724("724", CountryCode.BR),

    /**
     * 注释未完善
     */
    MCC_364("364", CountryCode.BS),

    /**
     * 注释未完善
     */
    MCC_402("402", CountryCode.BT),

    /**
     * 注释未完善
     */
    MCC_652("652", CountryCode.BW),

    /**
     * 注释未完善
     */
    MCC_257("257", CountryCode.BY),

    /**
     * 注释未完善
     */
    MCC_702("702", CountryCode.BZ),

    /**
     * 注释未完善
     */
    MCC_302("302", CountryCode.CA),

    /**
     * 注释未完善
     */
    MCC_630("630", CountryCode.CD),

    /**
     * 注释未完善
     */
    MCC_623("623", CountryCode.CF),

    /**
     * 注释未完善
     */
    MCC_629("629", CountryCode.CG),

    /**
     * 注释未完善
     */
    MCC_228("228", CountryCode.CH),

    /**
     * 注释未完善
     */
    MCC_612("612", CountryCode.CI),

    /**
     * 注释未完善
     */
    MCC_548("548", CountryCode.CK),

    /**
     * 注释未完善
     */
    MCC_730("730", CountryCode.CL),

    /**
     * 注释未完善
     */
    MCC_624("624", CountryCode.CM),

    /**
     * 中国大陆
     */
    MCC_460("460", CountryCode.CN),

    /**
     * 中国大陆
     */
    MCC_461("461", CountryCode.CN),

    /**
     * 注释未完善
     */
    MCC_732("732", CountryCode.CO),

    /**
     * 注释未完善
     */
    MCC_712("712", CountryCode.CR),

    /**
     * 注释未完善
     */
    MCC_368("368", CountryCode.CU),

    /**
     * 注释未完善
     */
    MCC_625("625", CountryCode.CV),

    /**
     * 注释未完善
     */
    MCC_280("280", CountryCode.CY),

    /**
     * 注释未完善
     */
    MCC_230("230", CountryCode.CZ),

    /**
     * 注释未完善
     */
    MCC_262("262", CountryCode.DE),

    /**
     * 注释未完善
     */
    MCC_638("638", CountryCode.DJ),

    /**
     * 注释未完善
     */
    MCC_238("238", CountryCode.DK),

    /**
     * 注释未完善
     */
    MCC_366("366", CountryCode.DM),

    /**
     * 注释未完善
     */
    MCC_370("370", CountryCode.DO),

    /**
     * 注释未完善
     */
    MCC_603("603", CountryCode.DZ),

    /**
     * 注释未完善
     */
    MCC_740("740", CountryCode.EC),

    /**
     * 注释未完善
     */
    MCC_248("248", CountryCode.EE),

    /**
     * 注释未完善
     */
    MCC_602("602", CountryCode.EG),

    /**
     * 注释未完善
     */
    MCC_657("657", CountryCode.ER),

    /**
     * 注释未完善
     */
    MCC_214("214", CountryCode.ES),

    /**
     * 注释未完善
     */
    MCC_636("636", CountryCode.ET),

    /**
     * 注释未完善
     */
    MCC_244("244", CountryCode.FI),

    /**
     * 注释未完善
     */
    MCC_542("542", CountryCode.FJ),

    /**
     * 注释未完善
     */
    MCC_750("750", CountryCode.FK),

    /**
     * 注释未完善
     */
    MCC_550("550", CountryCode.FM),

    /**
     * 注释未完善
     */
    MCC_288("288", CountryCode.FO),

    /**
     * 注释未完善
     */
    MCC_208("208", CountryCode.FR),

    /**
     * 注释未完善
     */
    MCC_628("628", CountryCode.GA),

    /**
     * 注释未完善
     */
    MCC_234("234", CountryCode.GB, CountryCode.GG, CountryCode.IM, CountryCode.JE),

    /**
     * 注释未完善
     */
    MCC_235("235", CountryCode.GB),

    /**
     * 注释未完善
     */
    MCC_352("352", CountryCode.GD),

    /**
     * 注释未完善
     */
    MCC_282("282", CountryCode.GE),

    /**
     * 阿布哈兹
     * <p>
     * 大多数国家承认“阿布哈兹”为“格鲁吉亚”的一部分，且目前ISO-3166标准
     * 未分配该国家/地区的代号，故这里将对应的国家/地区设定为“格鲁吉亚”。
     */
    MCC_289("289", CountryCode.GE),

    /**
     * 注释未完善
     */
    MCC_742("742", CountryCode.GF),

    /**
     * 注释未完善
     */
    MCC_620("620", CountryCode.GH),

    /**
     * 注释未完善
     */
    MCC_266("266", CountryCode.GI),

    /**
     * 注释未完善
     */
    MCC_290("290", CountryCode.GL),

    /**
     * 注释未完善
     */
    MCC_607("607", CountryCode.GM),

    /**
     * 注释未完善
     */
    MCC_611("611", CountryCode.GN),

    /**
     * 注释未完善
     */
    MCC_627("627", CountryCode.GQ),

    /**
     * 注释未完善
     */
    MCC_202("202", CountryCode.GR),

    /**
     * 注释未完善
     */
    MCC_704("704", CountryCode.GT),

    /**
     * 注释未完善
     */
    MCC_310("310", CountryCode.GU, CountryCode.MP, CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_311("311", CountryCode.GU, CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_632("632", CountryCode.GW),

    /**
     * 注释未完善
     */
    MCC_738("738", CountryCode.GY),

    /**
     * 香港地区
     */
    MCC_454("454", CountryCode.HK),

    /**
     * 注释未完善
     */
    MCC_708("708", CountryCode.HN),

    /**
     * 注释未完善
     */
    MCC_219("219", CountryCode.HR),

    /**
     * 注释未完善
     */
    MCC_372("372", CountryCode.HT),

    /**
     * 注释未完善
     */
    MCC_216("216", CountryCode.HU),

    /**
     * 注释未完善
     */
    MCC_510("510", CountryCode.ID),

    /**
     * 注释未完善
     */
    MCC_272("272", CountryCode.IE),

    /**
     * 注释未完善
     */
    MCC_425("425", CountryCode.IL, CountryCode.PS),

    /**
     * 注释未完善
     */
    MCC_404("404", CountryCode.IN),

    /**
     * 注释未完善
     */
    MCC_405("405", CountryCode.IN),

    /**
     * 注释未完善
     */
    MCC_406("406", CountryCode.IN),

    /**
     * 注释未完善
     */
    MCC_995("995", CountryCode.IO),

    /**
     * 注释未完善
     */
    MCC_418("418", CountryCode.IQ),

    /**
     * 注释未完善
     */
    MCC_432("432", CountryCode.IR),

    /**
     * 注释未完善
     */
    MCC_274("274", CountryCode.IS),

    /**
     * 注释未完善
     */
    MCC_222("222", CountryCode.IT),

    /**
     * 注释未完善
     */
    MCC_338("338", CountryCode.JM),

    /**
     * 注释未完善
     */
    MCC_416("416", CountryCode.JO),

    /**
     * 注释未完善
     */
    MCC_440("440", CountryCode.JP),

    /**
     * 注释未完善
     */
    MCC_441("441", CountryCode.JP),

    /**
     * 注释未完善
     */
    MCC_639("639", CountryCode.KE),

    /**
     * 注释未完善
     */
    MCC_437("437", CountryCode.KG),

    /**
     * 注释未完善
     */
    MCC_456("456", CountryCode.KH),

    /**
     * 注释未完善
     */
    MCC_545("545", CountryCode.KI),

    /**
     * 注释未完善
     */
    MCC_654("654", CountryCode.KM),

    /**
     * 注释未完善
     */
    MCC_356("356", CountryCode.KN),

    /**
     * 注释未完善
     */
    MCC_467("467", CountryCode.KP),

    /**
     * 注释未完善
     */
    MCC_450("450", CountryCode.KR),

    /**
     * 注释未完善
     */
    MCC_419("419", CountryCode.KW),

    /**
     * 注释未完善
     */
    MCC_346("346", CountryCode.KY),

    /**
     * 注释未完善
     */
    MCC_401("401", CountryCode.KZ),

    /**
     * 注释未完善
     */
    MCC_457("457", CountryCode.LA),

    /**
     * 注释未完善
     */
    MCC_415("415", CountryCode.LB),

    /**
     * 注释未完善
     */
    MCC_358("358", CountryCode.LC),

    /**
     * 注释未完善
     */
    MCC_295("295", CountryCode.LI),

    /**
     * 注释未完善
     */
    MCC_413("413", CountryCode.LK),

    /**
     * 注释未完善
     */
    MCC_618("618", CountryCode.LR),

    /**
     * 注释未完善
     */
    MCC_651("651", CountryCode.LS),

    /**
     * 注释未完善
     */
    MCC_246("246", CountryCode.LT),

    /**
     * 注释未完善
     */
    MCC_270("270", CountryCode.LU),

    /**
     * 注释未完善
     */
    MCC_247("247", CountryCode.LV),

    /**
     * 注释未完善
     */
    MCC_606("606", CountryCode.LY),

    /**
     * 注释未完善
     */
    MCC_604("604", CountryCode.MA),

    /**
     * 注释未完善
     */
    MCC_212("212", CountryCode.MC),

    /**
     * 注释未完善
     */
    MCC_259("259", CountryCode.MD),

    /**
     * 注释未完善
     */
    MCC_297("297", CountryCode.ME),

    /**
     * 注释未完善
     */
    MCC_646("646", CountryCode.MG),

    /**
     * 注释未完善
     */
    MCC_551("551", CountryCode.MH),

    /**
     * 注释未完善
     */
    MCC_294("294", CountryCode.MK),

    /**
     * 注释未完善
     */
    MCC_610("610", CountryCode.ML),

    /**
     * 注释未完善
     */
    MCC_414("414", CountryCode.MM),

    /**
     * 注释未完善
     */
    MCC_428("428", CountryCode.MN),

    /**
     * 澳门地区
     */
    MCC_455("455", CountryCode.MO),

    /**
     * 注释未完善
     */
    MCC_609("609", CountryCode.MR),

    /**
     * 注释未完善
     */
    MCC_354("354", CountryCode.MS),

    /**
     * 注释未完善
     */
    MCC_278("278", CountryCode.MT),

    /**
     * 注释未完善
     */
    MCC_617("617", CountryCode.MU),

    /**
     * 注释未完善
     */
    MCC_472("472", CountryCode.MV),

    /**
     * 注释未完善
     */
    MCC_650("650", CountryCode.MW),

    /**
     * 注释未完善
     */
    MCC_334("334", CountryCode.MX),

    /**
     * 注释未完善
     */
    MCC_502("502", CountryCode.MY),

    /**
     * 注释未完善
     */
    MCC_643("643", CountryCode.MZ),

    /**
     * 注释未完善
     */
    MCC_649("649", CountryCode.NA),

    /**
     * 注释未完善
     */
    MCC_546("546", CountryCode.NC),

    /**
     * 注释未完善
     */
    MCC_614("614", CountryCode.NE),

    /**
     * 注释未完善
     */
    MCC_621("621", CountryCode.NG),

    /**
     * 注释未完善
     */
    MCC_710("710", CountryCode.NI),

    /**
     * 注释未完善
     */
    MCC_204("204", CountryCode.NL),

    /**
     * 注释未完善
     */
    MCC_242("242", CountryCode.NO),

    /**
     * 注释未完善
     */
    MCC_429("429", CountryCode.NP),

    /**
     * 注释未完善
     */
    MCC_536("536", CountryCode.NR),

    /**
     * 注释未完善
     */
    MCC_555("555", CountryCode.NU),

    /**
     * 注释未完善
     */
    MCC_530("530", CountryCode.NZ),

    /**
     * 注释未完善
     */
    MCC_422("422", CountryCode.OM),

    /**
     * 注释未完善
     */
    MCC_714("714", CountryCode.PA),

    /**
     * 注释未完善
     */
    MCC_716("716", CountryCode.PE),

    /**
     * 注释未完善
     */
    MCC_547("547", CountryCode.PF),

    /**
     * 注释未完善
     */
    MCC_537("537", CountryCode.PG),

    /**
     * 注释未完善
     */
    MCC_515("515", CountryCode.PH),

    /**
     * 注释未完善
     */
    MCC_410("410", CountryCode.PK),

    /**
     * 注释未完善
     */
    MCC_260("260", CountryCode.PL),

    /**
     * 注释未完善
     */
    MCC_308("308", CountryCode.PM),

    /**
     * 注释未完善
     */
    MCC_330("330", CountryCode.PR),

    /**
     * 注释未完善
     */
    MCC_268("268", CountryCode.PT),

    /**
     * 注释未完善
     */
    MCC_552("552", CountryCode.PW),

    /**
     * 注释未完善
     */
    MCC_744("744", CountryCode.PY),

    /**
     * 注释未完善
     */
    MCC_427("427", CountryCode.QA),

    /**
     * 注释未完善
     */
    MCC_647("647", CountryCode.RE, CountryCode.YT),

    /**
     * 注释未完善
     */
    MCC_226("226", CountryCode.RO),

    /**
     * 注释未完善
     */
    MCC_220("220", CountryCode.RS),

    /**
     * 注释未完善
     */
    MCC_250("250", CountryCode.RU),

    /**
     * 注释未完善
     */
    MCC_635("635", CountryCode.RW),

    /**
     * 注释未完善
     */
    MCC_420("420", CountryCode.SA),

    /**
     * 注释未完善
     */
    MCC_540("540", CountryCode.SB),

    /**
     * 注释未完善
     */
    MCC_633("633", CountryCode.SC),

    /**
     * 注释未完善
     */
    MCC_634("634", CountryCode.SD),

    /**
     * 注释未完善
     */
    MCC_240("240", CountryCode.SE),

    /**
     * 注释未完善
     */
    MCC_525("525", CountryCode.SG),

    /**
     * 注释未完善
     */
    MCC_658("658", CountryCode.SH),

    /**
     * 注释未完善
     */
    MCC_293("293", CountryCode.SI),

    /**
     * 注释未完善
     */
    MCC_231("231", CountryCode.SK),

    /**
     * 注释未完善
     */
    MCC_619("619", CountryCode.SL),

    /**
     * 注释未完善
     */
    MCC_292("292", CountryCode.SM),

    /**
     * 注释未完善
     */
    MCC_608("608", CountryCode.SN),

    /**
     * 注释未完善
     */
    MCC_637("637", CountryCode.SO),

    /**
     * 注释未完善
     */
    MCC_746("746", CountryCode.SR),

    /**
     * 注释未完善
     */
    MCC_659("659", CountryCode.SS),

    /**
     * 注释未完善
     */
    MCC_626("626", CountryCode.ST),

    /**
     * 注释未完善
     */
    MCC_706("706", CountryCode.SV),

    /**
     * 注释未完善
     */
    MCC_417("417", CountryCode.SY),

    /**
     * 注释未完善
     */
    MCC_653("653", CountryCode.SZ),

    /**
     * 注释未完善
     */
    MCC_376("376", CountryCode.TC),

    /**
     * 注释未完善
     */
    MCC_622("622", CountryCode.TD),

    /**
     * 注释未完善
     */
    MCC_615("615", CountryCode.TG),

    /**
     * 注释未完善
     */
    MCC_520("520", CountryCode.TH),

    /**
     * 注释未完善
     */
    MCC_436("436", CountryCode.TJ),

    /**
     * 注释未完善
     */
    MCC_554("554", CountryCode.TK),

    /**
     * 注释未完善
     */
    MCC_514("514", CountryCode.TL),

    /**
     * 注释未完善
     */
    MCC_438("438", CountryCode.TM),

    /**
     * 注释未完善
     */
    MCC_605("605", CountryCode.TN),

    /**
     * 注释未完善
     */
    MCC_539("539", CountryCode.TO),

    /**
     * 注释未完善
     */
    MCC_286("286", CountryCode.TR),

    /**
     * 注释未完善
     */
    MCC_374("374", CountryCode.TT),

    /**
     * 注释未完善
     */
    MCC_553("553", CountryCode.TV),

    /**
     * 台湾省
     */
    MCC_466("466", CountryCode.TW),

    /**
     * 注释未完善
     */
    MCC_640("640", CountryCode.TZ),

    /**
     * 注释未完善
     */
    MCC_255("255", CountryCode.UA),

    /**
     * 注释未完善
     */
    MCC_641("641", CountryCode.UG),

    /**
     * 注释未完善
     */
    MCC_312("312", CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_313("313", CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_314("314", CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_315("315", CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_316("316", CountryCode.US),

    /**
     * 注释未完善
     */
    MCC_748("748", CountryCode.UY),

    /**
     * 注释未完善
     */
    MCC_434("434", CountryCode.UZ),

    /**
     * 注释未完善
     */
    MCC_360("360", CountryCode.VC),

    /**
     * 注释未完善
     */
    MCC_734("734", CountryCode.VE),

    /**
     * 注释未完善
     */
    MCC_348("348", CountryCode.VG),

    /**
     * 注释未完善
     */
    MCC_332("332", CountryCode.VI),

    /**
     * 注释未完善
     */
    MCC_452("452", CountryCode.VN),

    /**
     * 注释未完善
     */
    MCC_541("541", CountryCode.VU),

    /**
     * 注释未完善
     */
    MCC_543("543", CountryCode.WF),

    /**
     * 注释未完善
     */
    MCC_549("549", CountryCode.WS),

    /**
     * 科索沃
     * <p>
     * 大多数国家承认“科索沃”为“塞尔维亚”的一部分，且目前ISO-3166标准
     * 未分配该国家/地区的代号，故这里将对应的国家/地区设定为“塞尔维亚”。
     */
    MCC_221("221", CountryCode.RS),

    /**
     * 注释未完善
     */
    MCC_421("421", CountryCode.YE),

    /**
     * 注释未完善
     */
    MCC_655("655", CountryCode.ZA),

    /**
     * 注释未完善
     */
    MCC_645("645", CountryCode.ZM),

    /**
     * 注释未完善
     */
    MCC_648("648", CountryCode.ZW),

    // International operators

    /**
     * 国际运营商
     */
    MCC_901("901"),

    /**
     * 国际运营商
     */
    MCC_902("902"),

    /**
     * 国际运营商
     */
    MCC_991("991");

    /**
     * MCC值
     *
     * <table class="striped">
     * <caption>MCC首位数字含义</caption>
     * <thead>
     *     <tr><th scope="col">首位值</th><th scope="col">地理区域</th></tr>
     * </thead>
     * <tbody>
     *     <tr><th scope="row">0</th><td>测试网络</td></tr>
     *     <tr><th scope="row">2</th><td>欧洲</td></tr>
     *     <tr><th scope="row">3</th><td>北美和加勒比</td></tr>
     *     <tr><th scope="row">4</th><td>亚洲和中东</td></tr>
     *     <tr><th scope="row">5</th><td>澳大利亚和大洋洲</td></tr>
     *     <tr><th scope="row">6</th><td>非洲</td></tr>
     *     <tr><th scope="row">7</th><td>南美洲和中美洲</td></tr>
     *     <tr><th scope="row">9</th><td>全球（卫星、空中/飞机上、海上/船上、南极洲）</td></tr>
     * </tbody>
     * </table>
     */
    public final String code;

    /**
     * 国家或地区
     * <p>
     * 大多数MCC对应的国家或地区数组长度为1，存在个别一个MCC对应多个国家或地区的情况；
     * 对于测试网络（Test networks）、国际运营商（International operators）类别
     * 不存在国家或地区，即数组长度为0。
     */
    final CountryCode[] countriesOrSubdivisions;

    MobileCountryCode(String code, CountryCode... countriesOrSubdivisions) {
        this.code = code;
        this.countriesOrSubdivisions = countriesOrSubdivisions;
    }

    /**
     * 获取当前MCC对应的国家或地区
     *
     * @return 国家或地区
     */
    public CountryCode[] countriesOrSubdivisions() {
        return countriesOrSubdivisions;
    }

}
