package org.ximinghui.telecom.core.standard;

import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * 国际电话区号【部分实现】
 * <p>
 * 由国际电信联盟电信标准化部门(<a href="https://en.wikipedia.org/wiki/ITU-T">ITU-T</a>)定义，详见<a href="https://en.wikipedia.org/wiki/List_of_country_calling_codes">维基百科</a>
 */
@ToString
@AllArgsConstructor
public enum CountryCallingCode {

    /**
     * 中国大陆
     */
    CN(CountryCode.CN, 86), // 898604A4192180550375
    CN_2(CountryCode.CN, 87),

    /**
     * 香港特别行政区
     */
    HK(CountryCode.HK, 852),

    /**
     * 澳门特别行政区
     */
    MO(CountryCode.MO, 853),

    /**
     * 中国台湾省
     */
    TW(CountryCode.TW, 886),

    /**
     * 越南
     */
    VN(CountryCode.VN, 84),

    /**
     * 新加坡
     */
    SG(CountryCode.SG, 65);

    /**
     * 国家或地区
     */
    public final CountryCode countryOrSubdivision;

    /**
     * 区号代码
     */
    public final int code;

    public static CountryCallingCode of(int code) {
        CountryCallingCode[] all = CountryCallingCode.values();
        for (CountryCallingCode c : all) if (c.code == code) return c;
        throw new IllegalArgumentException("参数code无法识别：" + code);
    }

    public static CountryCallingCode of(String code) {
        try {
            return of(Integer.parseInt(code));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("参数code格式不正确：" + code);
        }
    }

    public static CountryCallingCode of(CountryCode country) {
        CountryCallingCode[] all = CountryCallingCode.values();
        for (CountryCallingCode c : all) if (c.countryOrSubdivision == country) return c;
        throw new IllegalArgumentException("参数country无法识别：" + country);
    }

}
