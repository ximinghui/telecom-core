package org.ximinghui.telecom.core.standard;

import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * 国家/地区代号
 * <p>
 * 基于ISO 3166-1 alpha-2标准
 */
@ToString
@AllArgsConstructor
public enum CountryCode {

    AD("Andorra", "安道尔"),

    AE("United Arab Emirates", "阿拉伯联合酋长国"),

    AF("Afghanistan", "阿富汗"),

    AG("Antigua and Barbuda", "安提瓜和巴布达"),

    AI("Anguilla", "安圭拉"),

    AL("Albania", "阿尔巴尼亚"),

    AM("Armenia", "亚美尼亚"),

    AO("Angola", "安哥拉"),

    AQ("Antarctica", "南极洲"),

    AR("Argentina", "阿根廷"),

    AS("American Samoa", "美属萨摩亚"),

    AT("Austria", "奥地利"),

    AU("Australia", "澳大利亚"),

    AW("Aruba", "阿鲁巴"),

    /**
     * Åland Islands
     */
    AX(CountryCode.NO_ENGLISH_NAME, "奥兰群岛"),

    AZ("Azerbaijan", "阿塞拜疆"),

    BA("Bosnia and Herzegovina", "波斯尼亚和黑塞哥维那"),

    BB("Barbados", "巴巴多斯"),

    BD("Bangladesh", "孟加拉国"),

    BE("Belgium", "未完善"),

    BF("Burkina Faso", "未完善"),

    BG("Bulgaria", "未完善"),

    BH("Bahrain", "未完善"),

    BI("Burundi", "未完善"),

    BJ("Benin", "未完善"),

    /**
     * Saint Barthélemy
     */
    BL(CountryCode.NO_ENGLISH_NAME, "圣巴泰勒米集体"),

    BM("Bermuda", "未完善"),

    BN("Brunei Darussalam", "未完善"),

    BO("Bolivia (Plurinational State of)", "未完善"),

    BQ("Bonaire, Sint Eustatius and Saba", "未完善"),

    BR("Brazil", "未完善"),

    BS("Bahamas", "未完善"),

    BT("Bhutan", "未完善"),

    BV("Bouvet Island", "未完善"),

    BW("Botswana", "未完善"),

    BY("Belarus", "未完善"),

    BZ("Belize", "未完善"),

    CA("Canada", "未完善"),

    CC("Cocos (Keeling) Islands", "未完善"),

    CD("Congo, Democratic Republic of the", "未完善"),

    CF("Central African Republic", "未完善"),

    CG("Congo", "未完善"),

    CH("Switzerland", "未完善"),

    /**
     * Côte d'Ivoire
     */
    CI(CountryCode.NO_ENGLISH_NAME, "科特迪瓦"),

    CK("Cook Islands", "未完善"),

    CL("Chile", "未完善"),

    CM("Cameroon", "未完善"),

    CN("China", "中国"),

    CO("Colombia", "未完善"),

    CR("Costa Rica", "未完善"),

    CU("Cuba", "未完善"),

    CV("Cabo Verde", "未完善"),

    /**
     * Curaçao
     */
    CW(CountryCode.NO_ENGLISH_NAME, "库拉索"),

    CX("Christmas Island", "未完善"),

    CY("Cyprus", "未完善"),

    CZ("Czechia", "未完善"),

    DE("Germany", "未完善"),

    DJ("Djibouti", "未完善"),

    DK("Denmark", "未完善"),

    DM("Dominica", "未完善"),

    DO("Dominican Republic", "未完善"),

    DZ("Algeria", "未完善"),

    EC("Ecuador", "未完善"),

    EE("Estonia", "未完善"),

    EG("Egypt", "未完善"),

    EH("Western Sahara", "未完善"),

    ER("Eritrea", "未完善"),

    ES("Spain", "未完善"),

    ET("Ethiopia", "未完善"),

    FI("Finland", "未完善"),

    FJ("Fiji", "未完善"),

    FK("Falkland Islands (Malvinas)", "未完善"),

    FM("Micronesia (Federated States of)", "未完善"),

    FO("Faroe Islands", "未完善"),

    FR("France", "未完善"),

    GA("Gabon", "未完善"),

    GB("United Kingdom of Great Britain and Northern Ireland", "未完善"),

    GD("Grenada", "未完善"),

    GE("Georgia", "未完善"),

    GF("French Guiana", "未完善"),

    GG("Guernsey", "未完善"),

    GH("Ghana", "未完善"),

    GI("Gibraltar", "未完善"),

    GL("Greenland", "未完善"),

    GM("Gambia", "未完善"),

    GN("Guinea", "未完善"),

    GP("Guadeloupe", "未完善"),

    GQ("Equatorial Guinea", "未完善"),

    GR("Greece", "未完善"),

    GS("South Georgia and the South Sandwich Islands", "未完善"),

    GT("Guatemala", "未完善"),

    GU("Guam", "未完善"),

    GW("Guinea-Bissau", "未完善"),

    GY("Guyana", "未完善"),

    HK("Hong Kong", "香港"),

    HM("Heard Island and McDonald Islands", "未完善"),

    HN("Honduras", "未完善"),

    HR("Croatia", "未完善"),

    HT("Haiti", "未完善"),

    HU("Hungary", "未完善"),

    ID("Indonesia", "未完善"),

    IE("Ireland", "未完善"),

    IL("Israel", "未完善"),

    IM("Isle of Man", "未完善"),

    IN("India", "未完善"),

    IO("British Indian Ocean Territory", "未完善"),

    IQ("Iraq", "未完善"),

    IR("Iran (Islamic Republic of)", "未完善"),

    IS("Iceland", "未完善"),

    IT("Italy", "未完善"),

    JE("Jersey", "未完善"),

    JM("Jamaica", "未完善"),

    JO("Jordan", "未完善"),

    JP("Japan", "未完善"),

    KE("Kenya", "未完善"),

    KG("Kyrgyzstan", "未完善"),

    KH("Cambodia", "未完善"),

    KI("Kiribati", "未完善"),

    KM("Comoros", "未完善"),

    KN("Saint Kitts and Nevis", "未完善"),

    KP("Korea (Democratic People's Republic of)", "未完善"),

    KR("Korea, Republic of", "未完善"),

    KW("Kuwait", "未完善"),

    KY("Cayman Islands", "未完善"),

    KZ("Kazakhstan", "未完善"),

    LA("Lao People's Democratic Republic", "未完善"),

    LB("Lebanon", "未完善"),

    LC("Saint Lucia", "未完善"),

    LI("Liechtenstein", "未完善"),

    LK("Sri Lanka", "未完善"),

    LR("Liberia", "未完善"),

    LS("Lesotho", "未完善"),

    LT("Lithuania", "未完善"),

    LU("Luxembourg", "未完善"),

    LV("Latvia", "未完善"),

    LY("Libya", "未完善"),

    MA("Morocco", "未完善"),

    MC("Monaco", "未完善"),

    MD("Moldova, Republic of", "未完善"),

    ME("Montenegro", "未完善"),

    MF("Saint Martin (French part)", "未完善"),

    MG("Madagascar", "未完善"),

    MH("Marshall Islands", "未完善"),

    MK("North Macedonia", "未完善"),

    ML("Mali", "未完善"),

    MM("Myanmar", "未完善"),

    MN("Mongolia", "未完善"),

    MO("Macao", "澳门"),

    MP("Northern Mariana Islands", "未完善"),

    MQ("Martinique", "未完善"),

    MR("Mauritania", "未完善"),

    MS("Montserrat", "未完善"),

    MT("Malta", "未完善"),

    MU("Mauritius", "未完善"),

    MV("Maldives", "未完善"),

    MW("Malawi", "未完善"),

    MX("Mexico", "未完善"),

    MY("Malaysia", "未完善"),

    MZ("Mozambique", "未完善"),

    NA("Namibia", "未完善"),

    NC("New Caledonia", "未完善"),

    NE("Niger", "未完善"),

    NF("Norfolk Island", "未完善"),

    NG("Nigeria", "未完善"),

    NI("Nicaragua", "未完善"),

    NL("Netherlands", "未完善"),

    NO("Norway", "未完善"),

    NP("Nepal", "未完善"),

    NR("Nauru", "未完善"),

    NU("Niue", "未完善"),

    NZ("New Zealand", "未完善"),

    OM("Oman", "未完善"),

    PA("Panama", "未完善"),

    PE("Peru", "未完善"),

    PF("French Polynesia", "未完善"),

    PG("Papua New Guinea", "未完善"),

    PH("Philippines", "未完善"),

    PK("Pakistan", "未完善"),

    PL("Poland", "未完善"),

    PM("Saint Pierre and Miquelon", "未完善"),

    PN("Pitcairn", "未完善"),

    PR("Puerto Rico", "未完善"),

    PS("Palestine, State of", "未完善"),

    PT("Portugal", "未完善"),

    PW("Palau", "未完善"),

    PY("Paraguay", "未完善"),

    QA("Qatar", "未完善"),

    /**
     * Réunion
     */
    RE(CountryCode.NO_ENGLISH_NAME, "留尼汪"),

    RO("Romania", "未完善"),

    RS("Serbia", "未完善"),

    RU("Russian Federation", "未完善"),

    RW("Rwanda", "未完善"),

    SA("Saudi Arabia", "未完善"),

    SB("Solomon Islands", "未完善"),

    SC("Seychelles", "未完善"),

    SD("Sudan", "未完善"),

    SE("Sweden", "未完善"),

    SG("Singapore", "新加坡"),

    SH("Saint Helena, Ascension and Tristan da Cunha", "未完善"),

    SI("Slovenia", "未完善"),

    SJ("Svalbard and Jan Mayen", "未完善"),

    SK("Slovakia", "未完善"),

    SL("Sierra Leone", "未完善"),

    SM("San Marino", "未完善"),

    SN("Senegal", "未完善"),

    SO("Somalia", "未完善"),

    SR("Suriname", "未完善"),

    SS("South Sudan", "未完善"),

    ST("Sao Tome and Principe", "未完善"),

    SV("El Salvador", "未完善"),

    SX("Sint Maarten (Dutch part)", "未完善"),

    SY("Syrian Arab Republic", "未完善"),

    SZ("Eswatini", "未完善"),

    TC("Turks and Caicos Islands", "未完善"),

    TD("Chad", "未完善"),

    TF("French Southern Territories", "未完善"),

    TG("Togo", "未完善"),

    TH("Thailand", "未完善"),

    TJ("Tajikistan", "未完善"),

    TK("Tokelau", "未完善"),

    TL("Timor-Leste", "未完善"),

    TM("Turkmenistan", "未完善"),

    TN("Tunisia", "未完善"),

    TO("Tonga", "未完善"),

    /**
     * Türkiye
     */
    TR(CountryCode.NO_ENGLISH_NAME, "土耳其"),

    TT("Trinidad and Tobago", "未完善"),

    TV("Tuvalu", "未完善"),

    TW("Taiwan, Province of China", "中国台湾省"),

    TZ("Tanzania, United Republic of", "未完善"),

    UA("Ukraine", "未完善"),

    UG("Uganda", "未完善"),

    UM("United States Minor Outlying Islands", "未完善"),

    US("United States of America", "未完善"),

    UY("Uruguay", "未完善"),

    UZ("Uzbekistan", "未完善"),

    VA("Holy See", "未完善"),

    VC("Saint Vincent and the Grenadines", "未完善"),

    VE("Venezuela (Bolivarian Republic of)", "未完善"),

    VG("Virgin Islands (British)", "未完善"),

    VI("Virgin Islands (U.S.)", "维尔京群岛（美国）"),

    VN("Viet Nam", "越南"),

    VU("Vanuatu", "未完善"),

    WF("Wallis and Futuna", "瓦利斯和富图纳"),

    WS("Samoa", "萨摩亚"),

    YE("Yemen", "也门"),

    YT("Mayotte", "马约特省"),

    ZA("South Africa", "南非"),

    ZM("Zambia", "赞比亚"),

    ZW("Zimbabwe", "津巴布韦");

    private static final String NO_ENGLISH_NAME = "NO ENGLISH NAME";

    public final String englishName;

    public final String chineseName;

}
